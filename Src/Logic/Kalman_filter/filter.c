#include "Logic/Kalman_filter/filter.h"
#include "Foundations/imu.h"
//#include "math.h"

#define LOOP_DELAY        5 //50 //+czas wykohywania kodu
#define INITIAL_DELAY    5
#define RADIUS            0.016
#define FLOAT_TO_INT    1000000
#define KALMAN_DIST     20
#define ENCODER_SCALAR  .008487654f

//Żyroskop+Acc
extern uint16_t IMU_rawdata_acc[3];
extern int16_t IMU_rawdata_gyro[3];
//Enkodery
extern TIM_HandleTypeDef htim3; //Prawy
extern TIM_HandleTypeDef htim4; //Lewy
//send data
extern UART_HandleTypeDef huart4;
extern uint8_t RX_data[UART_RX_BUFFOR_SIZE];
extern uint8_t TX_data[UART_TX_BUFFOR_SIZE]; // Tablica do wysylania
extern uint16_t TX_size;

//RZECZYWISTE / INTERPRETACJA
//Oś Z - w górę oś oś X
//Oś X - do przodu oś Y
//Oś Y - w prawo oś Z
//TX_size = sprintf (TX_data, "A_X = %i ; A_Y = %i ; A_Z = %i || G_R = %i ; G_P = %i ; G_Y = %i\n\r", IMU_rawdata[2], IMU_rawdata[0], IMU_rawdata[1], IMU_rawdata[5], IMU_rawdata[3], IMU_rawdata[4]);
//dawniej była jedna tablica IMU_rawdata, zostala podzielona na dwie tablice(acc i gyro), ze względu na roznice w typie danych

int getGyroZ(void) { return IMU_rawdata_gyro[1]; }

int getGyroX(void) { return IMU_rawdata_gyro[2]; }

int getGyroY(void) { return IMU_rawdata_gyro[0]; }

uint16_t getEncoderLeft(void) { return htim4.Instance->CNT; }

uint16_t getEncoderRight(void) { return htim3.Instance->CNT; }

int getAccX(void) { return IMU_rawdata_acc[2]; }

int getAccY(void) { return IMU_rawdata_acc[0]; }

void setDelay(int time) { HAL_Delay(time); }

void dataExtractor() {
    IMU_MPU6050_Init();
    IMU_MPU6050_ReadRaw(IMU_rawdata_acc, IMU_rawdata_gyro);



    //export data into a file (for variation of acc and enc)
    /*FILE *f = fopen("ACCandENC.txt", "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }*/
    int loopsCounterForDataExtractor = 0;


    Motors_Init();
    Motor_Right_PWM(105);
    Motor_Left_PWM(100);

    float temp1, temp2, temp3, temp4, temp5;
    Scalar accZ, accX, accY, yL, yR;

    while ((RX_data[0] != 'q') && loopsCounterForDataExtractor <= 25) {

        loopsCounterForDataExtractor++;
        IMU_MPU6050_ReadRaw(IMU_rawdata_acc, IMU_rawdata_gyro);
        accZ = (Scalar) (IMU_rawdata_acc[1]) * 9.80665f * .000061f;//rescale data
        accX = (Scalar) (IMU_rawdata_acc[2]) * 9.80665f * .000061f;
        accY = (Scalar) (IMU_rawdata_acc[0]) * 9.80665f * .000061f;
        yL = (Scalar) (htim4.Instance->CNT * 2) * .008487654f; //meassured distance
        yR = (Scalar) (htim3.Instance->CNT * 2) * .008487654f;
        HAL_Delay(LOOP_DELAY);

        // ENKODERY NORMALIZACJA - 3240 RAW to około 27,5 cm, 50 pętli - wspl - 0,008487654
        // ENKODERY NOMRLIAZCJA - 0.1518 cm to około 27,5 cm, 50 petli

        temp1 = accZ * FLOAT_TO_INT;
        temp2 = accX * FLOAT_TO_INT;
        temp3 = accY * FLOAT_TO_INT;
        temp4 = yL * FLOAT_TO_INT;
        temp5 = yR * FLOAT_TO_INT;
        TX_size = sprintf(TX_data, "ACCX %u.%03u \tACCY %u.%03u\tACCZ %u.%03u\tENCL %u.%03u\tENCR %u.%03u\n\r",
                          (int) temp2 / FLOAT_TO_INT, (int) temp2 % FLOAT_TO_INT, (int) temp3 / FLOAT_TO_INT,
                          (int) temp3 % FLOAT_TO_INT, (int) temp1 / FLOAT_TO_INT, (int) temp1 % FLOAT_TO_INT,
                          (int) temp4 / FLOAT_TO_INT, (int) temp4 % FLOAT_TO_INT, (int) temp5 / FLOAT_TO_INT,
                          (int) temp5 % FLOAT_TO_INT);
        //TX_size = sprintf(TX_data, "%u.%04u  \t%u.%04u  \t%u.%04u   \t%u.%04u  \t%u.%04u\n\r", (int)temp2 / FLOAT_TO_INT, (int)temp2 % FLOAT_TO_INT, (int)temp3 / FLOAT_TO_INT, (int)temp3 % FLOAT_TO_INT,(int)temp1 / FLOAT_TO_INT, (int)temp1 % FLOAT_TO_INT,(int)temp4 / FLOAT_TO_INT, (int)temp4 % FLOAT_TO_INT,(int)temp5 / FLOAT_TO_INT, (int)temp5 % FLOAT_TO_INT);
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(5);
    }

    Motors_Stop();
}

void routeEstimator() {
    // 1) Zapisac odczyt czujnikow
    // 2) Obrocic *recznie* robota
    // 3) Puscic Kalmana
    // 4) Czekac az dojedzie do ustalonej pozycji z pomocą PID-a.

    //Czujnii odbicia boczne i frontalne
    //uint16_t leftSensor[DIST_SMA_SAMPLES], rightSensor[DIST_SMA_SAMPLES];
    uint16_t frontLSensor[DIST_SMA_SAMPLES], frontRSensor[DIST_SMA_SAMPLES];
    //uint16_t leftSMAsum, rightSMAsum, rightSMA, leftSMA;
    uint16_t frontLSMAsum, frontRSMAsum, frontLSMA, frontRSMA, avgFront;
    //uint16_t rightZero, leftZero;
    uint16_t frontLZero, frontRZero, avgZero;

    //Inicjalizacja tablic pomiarami
//    leftSMAsum		= DistSensorSMAInit(&leftSensor,  ADC_DIST_LEFT_SIDE);
//    rightSMAsum		= DistSensorSMAInit(&rightSensor, ADC_DIST_RIGHT_SIDE);
    frontLSMAsum = DistSensorSMAInit(frontLSensor, ADC_DIST_LEFT_FORWARD);
    frontRSMAsum = DistSensorSMAInit(frontRSensor, ADC_DIST_RIGHT_FORWARD);
//	diagLeftSMAsum	= DistSensorSMAInit(&diagLeftSensor, ADC_DIST_LEFT_DIAGONAL);
//	diagRightSMAsum = DistSensorSMAInit(&diagRightSensor, ADC_DIST_RIGHT_DIAGONAL);
//    rightSMA	    = DistSensorSMAStep(&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
//    leftSMA		    = DistSensorSMAStep(&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
//    diagLeftSMA     = DistSensorSMAStep(&diagLeftSensor, &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//    diagRightSMA    = DistSensorSMAStep(&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
    frontLSMA = DistSensorSMAStep(frontLSensor, &frontLSMAsum, ADC_DIST_LEFT_FORWARD);
    frontRSMA = DistSensorSMAStep(frontRSensor, &frontRSMAsum, ADC_DIST_RIGHT_FORWARD);
    avgZero = (frontLSMA + frontRSMA) * 0.5;

    avgFront = 0;

    //Kalibraca (pozycja startowa)
    //leftZero		= leftSMA;//DistSensorSMAStep(&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
    //rightZero		= rightSMA;//DistSensorSMAStep(&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
    //diagLeftZero	= DistSensorSMAStep(&diagLeftSensor,  &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
    //diagRightZero	= DistSensorSMAStep(&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
    //avgZero			= (leftZero + rightZero) * 0.5;
    //diagAvgZero		= (diagLeftZero + diagRightZero) * 0.5;

    //Enkodery
    uint16_t encoderRight, encoderLeft;
    int err = 0;

    Motors_Turn_Around();
    //Motors_Stop();
    //HAL_Delay(100);
    Motors_Init();

    //htim4.Instance->CNT = 0;
    //htim3.Instance->CNT = 0;

    UART_Send_String(">wchodze do routeEstimator\n\r");
    IMU_MPU6050_Init();
    IMU_MPU6050_ReadRaw(IMU_rawdata_acc, IMU_rawdata_gyro);

    //declaration and allocation:
    Scalar dt = 0.5; //500ms dla 0.5 -> około 1,8 cm dla 0.2 -> 6.5 0.3->5.1 (kalman od 5,14 do 5,9) 0.29 -> 4.6 dla stdV = 5
    //dla stdV = 2: 0.3 -> 6.1 2.5 - > dość dobrze, min 2mm max 9mm odchyłu przy dłuższych drogach mniej fajnie, 2.7 ->
    //0.2

    //matrixes: A 2x2, B 2x1, C 1x2
    Matrix A = allocateMatrix();
    Vector B = allocateVector();
    RowVector C = (RowVector) allocateVector();

    //pPrev, pNext, V 2x2, W 1x1, xPrex 2x1
    Scalar stdDev_v = 50;
    Scalar stdDev_w = 2;
    Matrix V = allocateMatrix();
    Scalar W = stdDev_v * stdDev_v * dt;
    Matrix pPrev = allocateMatrix();
    Matrix pNext = allocateMatrix();
    Vector xPrev = allocateVector();

    //variables for meassurements actualisation
    Scalar accX, accY, gyroX, gyroY;
    Vector xNext = allocateVector();
    Vector K = allocateVector();
    Scalar eps = 0;
    Scalar S = 0;
    Scalar u = 0;
    Scalar y = getEncoderRight() * ENCODER_SCALAR;
    Scalar y_last = 0;

    //absolute distance
    Scalar absoluteDistance = 0;

    //s(k) = s(k-1) + v(k-1) * dt + 0.5*u(k-1)*t^2 + sNoise; v(k) = v(k-1) + t*u(k-1) + vNoise:
    setMatrix(A, 1, 0, 0, 0.5);
    setVector(B, 0.5 * dt * dt, dt);

    //C = [route, velocity]
    setVector(C, 1, 0);
    setMatrix(V, stdDev_v * stdDev_v * dt, 0, 0, stdDev_v * stdDev_v * dt);

    //initial filter values - identity matrix
    setMatrix(pNext, 1, 0, 0, 1);

    Scalar currentStepDistance = 0;

    //initial delay of the filter (the same as the microcontroler) here
    HAL_Delay(INITIAL_DELAY);

    //gyroX gyroY <- get current velocity here
    gyroX = IMU_rawdata_gyro[2] * .007633f; //* 250 / 32768; //rescale data
    gyroY = IMU_rawdata_gyro[0] * .007633f; //250 / 32768;

    //initial route is zero //velocity abs value
    setVector(xNext, 0, 0); //sqrt(pow(gyroX,2)+pow(gyroY,2)); //na poczatku testowac tylko z X\Y


    IMU_MPU6050_Init();

    //TEMP VALUES
    Vector tempVector1 = allocateVector();
    Vector tempVector2 = allocateVector();
    Matrix tempMatrix1 = allocateMatrix();
    Matrix tempMatrix2 = allocateMatrix();
    Matrix tempMatrix3 = allocateMatrix();

    UART_Send_String(">wchodzę do while\n\r");


    htim4.Instance->CNT = 0;
    //HAL_Delay(10);
    htim3.Instance->CNT = 0;
    //HAL_Delay(10);

    //Calculations made every 100ms
    if (A != NULL && B != NULL && C != NULL && V != NULL && pPrev != NULL && pNext != NULL && xPrev != NULL &&
        xNext != NULL && K != NULL &&
        tempVector1 != NULL && tempVector2 != NULL && tempMatrix1 != NULL && tempMatrix2 != NULL &&
        tempMatrix3 != NULL) {
        while (RX_data[0] != 'q' && (avgFront - avgZero < 300)) {
            IMU_MPU6050_ReadRaw(IMU_rawdata_acc, IMU_rawdata_gyro);

            currentStepDistance = 0;
            // x(t+1|t) = Ax(t|t) + Bu(t)
            accX = IMU_rawdata_acc[2] * 9.80665f * .000061f;//* 4 / 65535; //rescale data
            accY = IMU_rawdata_acc[1] * 9.80665f *
                   .000061f;// * 4 / 65535; tymczasowo zamienione na Z, bo to chyba oś boczna
            u = sqrt(pow(accX, 2) + pow(accY, 2)); //sqrt(pow(accX,2)+pow(accY,2)); //na poczatku testowac tylko z X\Y)
            matrixMultVector(A, xNext, tempVector1); //A_xNext = tempVector1
            vectorMultScalar(B, u, tempVector2); //B_u = tempVector2
            vectorAdd(tempVector1, tempVector2, xPrev);

            // P(t+1|t) = AP(t|t)A^T + V
            matrixMultMatrix(A, pNext, tempMatrix1); //A_pNext = tempMatrix1
            matrixTranspose(A, tempMatrix2); //ATrans = tempMatrix2
            matrixMultMatrix(tempMatrix1, tempMatrix2, tempMatrix3); // A_pNext_ATrans = tempMatrix3
            matrixAdd(tempMatrix3, V, pPrev);


            // eps(t) = y(t) - Cx(t|t-1)

            y = getEncoderRight() * ENCODER_SCALAR; //meassured distance
            eps = y - y_last - rowVectorMultVector(C, xPrev);


            // S(t) = CP(t|t-1)C^T + W
            rowVectorMultMarix(C, pPrev, tempVector1); //RowVector C_pPrev = tempVector1
            setVector(tempVector2, C[0], C[1]); //Vector CTrans = tempVector2
            S = rowVectorMultVector(tempVector1, tempVector2) + W;


            // K(t) = P(t|t-1)C^TS(t)^-1
            matrixMultVector(pPrev, tempVector2, tempVector1); //pPrev_CTrans = tempVector1
            vectorMultScalar(tempVector1, 1 / S, K);

            // x(t|t) = x(t|t-1) + K(t)eps(t)
            //matrixMultVector(pPrev, tempVector2, tempVector1)
            vectorMultScalar(K, eps, tempVector1); // Matrix K_eps = tempMatrix1
            vectorAdd(xPrev, tempVector1, xNext);

            // P(t|t) = P(t|t-1) - K(t)S(t)K(t)^T
            vectorMultScalar(K, S, tempVector1); // Vector K_S = tempVector1
            //matrixTranspose(K,tempMatrix2); // RowVector KTrans = tempMatrix2
            vectorMultRowVector(tempVector1, (RowVector) K, tempMatrix1); //Matrix K_S_KTrans = tempMatrix1
            matrixSub(pPrev, tempMatrix1, pNext);

            //add distance covered in this step to the absolute distance
            currentStepDistance = rowVectorMultVector(C, xNext);//(y - y_last) *2;

            absoluteDistance += currentStepDistance; //czy na pewno jest to wartość w kolejnym kroku pętli czy też absolutna?

//            TX_size = sprintf (TX_data, "A_X = %i \t A_Y = %i \t A_Z = %i \t||\t G_R = %i \t G_P = %i \t G_Y = %i \t || ENC=%i\n\r", IMU_rawdata[2], IMU_rawdata[0], IMU_rawdata[1], IMU_rawdata[5], IMU_rawdata[3], IMU_rawdata[4], htim4.Instance->CNT);
//            HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
//            HAL_Delay(100);

            //while(!Btn_IsPressed(BTN1_Pin)){}
            /*float temp1 = absoluteDistance*FLOAT_TO_INT;
            float temp2 = (y - y_last )*FLOAT_TO_INT;
            float temp3 = currentStepDistance*FLOAT_TO_INT;
            float temp4 = (y - y_last -  currentStepDistance)*FLOAT_TO_INT;
            float temp5 = eps*FLOAT_TO_INT;
            //float temp1 = 1.234;
            //UART_Send_String("<.>\n\r");
*/
            //TX_size = sprintf(TX_data, "abs. dist. = %u.%03u \t\t| meas. dist. = %u.%03u \t\t| est. dist. = %u.%03u \t\t| meas. adj = %u.%03u \n\r", absoluteDistance, y, currentStepDistance, y - currentStepDistance);
            /*TX_size = sprintf(TX_data, "abs. dist. = %u.%03u \t| meas. dist. = %u.%03u \t| est. dist. = %u.%03u \t| meas. adj = %u.%03u \t| eps = %u.%03u\n\r", (int)temp1 / FLOAT_TO_INT, (int)temp1 % FLOAT_TO_INT, (int)temp2 / FLOAT_TO_INT, (int)temp2 % FLOAT_TO_INT, (int)temp3 / FLOAT_TO_INT, (int)temp3 % FLOAT_TO_INT, (int)temp4 / FLOAT_TO_INT, (int)temp4 % FLOAT_TO_INT, (int)temp5 / FLOAT_TO_INT, (int)temp5 % FLOAT_TO_INT);

			//TX_size = UART_TX_BUFFOR_SIZE;
            //TX_size = sprintf(TX_data, "DATA SIZE = %i\n\r", TX_size); TX_size =
			HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
			HAL_Delay(5);*/
            //czy adjustment jest względem dystansu całkowitego (jeśli filtr mierzy całkowity) czy też względem krokowego (jeśli mierzy stan w danym kroku - skłaniam się ku tej opcji !sprawdzić!

            //while(!Btn_IsPressed(BTN3_Pin)){}

            //frontLSMA       = DistSensorSMAStep(&frontLSensor, &frontLSMAsum, ADC_DIST_LEFT_FORWARD);
            //frontRSMA       = DistSensorSMAStep(&frontRSensor, &frontRSMAsum, ADC_DIST_RIGHT_FORWARD);
            frontLSMA = DistSensorMeassure(ADC_DIST_LEFT_FORWARD);
            frontRSMA = DistSensorMeassure(ADC_DIST_RIGHT_FORWARD);
            avgFront = (frontLSMA + frontRSMA) * 0.5;
            //if((avgFront >= avgZero)){break;}

            //delay function here (to get new measurements, e.g. 100ms)
            //TX_size = sprintf(TX_data, "koniec petli nr = %i\n\r", counter);
            //HAL_UART_Transmit_IT(&uart4, TX_data, TX_size);
            y_last = y;

            encoderRight = (htim3.Instance->CNT) / 4;
            encoderLeft = (htim4.Instance->CNT) / 4;
            err = encoderLeft - encoderRight;
            Motors_PID((float) err);
            setDelay(LOOP_DELAY);
            //TX_size = sprintf(TX_data, "\terr=%i \tE_R=%i \tE_L=%i \n\r", err, encoderRight, encoderLeft);
            //HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
            //HAL_Delay(2);
        }
    }

    Motors_Stop();

    freeMatrix(&A);
    freeMatrix(&V);
    freeMatrix(&pPrev);
    freeMatrix(&pNext);
    freeVector(&B);
    freeVector(&C);
    freeVector(&xPrev);
    freeVector(&xNext);
    freeVector(&K);
    freeVector(&tempVector1);
    freeVector(&tempVector2);
    freeMatrix(&tempMatrix1);
    freeMatrix(&tempMatrix2);
    freeMatrix(&tempMatrix3);

    float temp1 = absoluteDistance * FLOAT_TO_INT;
    float temp2 = (y - y_last) * FLOAT_TO_INT;
    float temp3 = currentStepDistance * FLOAT_TO_INT;
    float temp4 = (y - y_last - currentStepDistance) * FLOAT_TO_INT;
    float temp5 = eps * FLOAT_TO_INT;

    TX_size = sprintf(TX_data,
                      "abs. dist. = %u.%03u \t| meas. dist. = %u.%03u \t| est. dist. = %u.%03u \t| meas. adj = %u.%03u \t| eps = %u.%03u\n\r",
                      (int) temp1 / FLOAT_TO_INT, (int) temp1 % FLOAT_TO_INT, (int) temp2 / FLOAT_TO_INT,
                      (int) temp2 % FLOAT_TO_INT, (int) temp3 / FLOAT_TO_INT, (int) temp3 % FLOAT_TO_INT,
                      (int) temp4 / FLOAT_TO_INT, (int) temp4 % FLOAT_TO_INT, (int) temp5 / FLOAT_TO_INT,
                      (int) temp5 % FLOAT_TO_INT);

    //TX_size = UART_TX_BUFFOR_SIZE;
    //TX_size = sprintf(TX_data, "DATA SIZE = %i\n\r", TX_size); TX_size =
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(5);


    UART_Send_String(">przejechałem KALMAN_DIST cm (Warunek Spelniony)\n\r");
}

void orientationEstimator() {
    UART_Send_String(">wchodze do orientationEstimator\n\r");
    IMU_MPU6050_Init();
    IMU_MPU6050_ReadRaw(IMU_rawdata_acc, IMU_rawdata_gyro);

    //declaration and allocation:
    Scalar dt = 0.1;

    //matrixes: A 2x2, B 2x1, C 1x2
    Matrix A = allocateMatrix();
    Vector B = allocateVector();
    RowVector C = (RowVector) allocateVector();

    //pPrev, pNext, V 2x2, W 1x1, xPrex 2x1
    Scalar stdDev_v = 1;
    Scalar stdDev_w = 2;
    Matrix V = allocateMatrix();
    Scalar W = stdDev_v * stdDev_v * dt;
    Matrix pPrev = allocateMatrix();
    Matrix pNext = allocateMatrix();
    Vector xPrev = allocateVector();

    //variables for meassurements actualisation
    Scalar accX, accY, gyroZ;
    Vector xNext = allocateVector();
    Vector K = allocateVector();
    Scalar eps = 0;
    Scalar S = 0;
    Scalar u = 0;
    Scalar y = 0;

    //absolute distance
    Scalar absoluteAngle = 0;

    //s(k) = s(k-1) + v(k-1) * dt + 0.5*u(k-1)*t^2 + sNoise; v(k) = v(k-1) + t*u(k-1) + vNoise:
    setMatrix(A, 1, 0, 0, 0.5);
    setVector(B, 0.5 * dt * dt, dt);

    //C = [route, velocity]
    setVector(C, 1, 0);
    setMatrix(V, stdDev_v * stdDev_v * dt, 0, 0, stdDev_v * stdDev_v * dt);

    //initial filter values - identity matrix
    setMatrix(pNext, 1, 0, 0, 1);

    Scalar currentAngle = 0;

    //initial delay of the filter (the same as the microcontroler) here
    HAL_Delay(INITIAL_DELAY);

    accX = getAccX() * 4 / 65535; //rescale data
    accY = getAccY() * 4 / 65535;
    setVector(xNext, atan(getAccX() / getAccY()) * 180 / M_PI, 0);

    IMU_MPU6050_Init();

    //TEMP VALUES
    Vector tempVector1 = allocateVector();
    Vector tempVector2 = allocateVector();
    Matrix tempMatrix1 = allocateMatrix();
    Matrix tempMatrix2 = allocateMatrix();
    Matrix tempMatrix3 = allocateMatrix();

    UART_Send_String(">wchodzę do while\n\r");


    //Calculations made every 100ms
    if (A != NULL && B != NULL && C != NULL && V != NULL && pPrev != NULL && pNext != NULL && xPrev != NULL &&
        xNext != NULL && K != NULL &&
        tempVector1 != NULL && tempVector2 != NULL && tempMatrix1 != NULL && tempMatrix2 != NULL &&
        tempMatrix3 != NULL) {
        while (RX_data[0] != 'q') {
            IMU_MPU6050_ReadRaw(IMU_rawdata_acc, IMU_rawdata_gyro);

            currentAngle = 0;
            // x(t+1|t) = Ax(t|t) + Bu(t)
            u = getGyroX() * 250 / 32768;;
            matrixMultVector(A, xNext, tempVector1); //A_xNext = tempVector1
            vectorMultScalar(B, u, tempVector2); //B_u = tempVector2
            vectorAdd(tempVector1, tempVector2, xPrev);

            // P(t+1|t) = AP(t|t)A^T + V
            matrixMultMatrix(A, pNext, tempMatrix1); //A_pNext = tempMatrix1
            matrixTranspose(A, tempMatrix2); //ATrans = tempMatrix2
            matrixMultMatrix(tempMatrix1, tempMatrix2, tempMatrix3); // A_pNext_ATrans = tempMatrix3
            matrixAdd(tempMatrix3, V, pPrev);

            // eps(t) = y(t) - Cx(t|t-1)
            accX = getAccX() * 4 / 65535; //rescale data
            accY = getAccY() * 4 / 65535;
            y = atan(getAccX() / getAccY()) * 180 / M_PI;
            eps = y - rowVectorMultVector(C, xPrev);

            // S(t) = CP(t|t-1)C^T + W
            rowVectorMultMarix(C, pPrev, tempVector1); //RowVector C_pPrev = tempVector1
            setVector(tempVector2, C[0], C[1]); //Vector CTrans = tempVector2
            S = rowVectorMultVector(tempVector1, tempVector2) + W;

            // K(t) = P(t|t-1)C^TS(t)^-1
            matrixMultVector(pPrev, tempVector2, tempVector1); //pPrev_CTrans = tempVector1
            vectorMultScalar(tempVector1, 1 / S, K);

            // x(t|t) = x(t|t-1) + K(t)eps(t)
            //matrixMultVector(pPrev, tempVector2, tempVector1)
            vectorMultScalar(K, eps, tempVector1); // Matrix K_eps = tempMatrix1
            vectorAdd(xPrev, tempVector1, xNext);

            // P(t|t) = P(t|t-1) - K(t)S(t)K(t)^T
            vectorMultScalar(K, S, tempVector1); // Vector K_S = tempVector1
            //matrixTranspose(K,tempMatrix2); // RowVector KTrans = tempMatrix2
            vectorMultRowVector(tempVector1, (RowVector) K, tempMatrix1); //Matrix K_S_KTrans = tempMatrix1
            matrixSub(pPrev, tempMatrix1, pNext);

            //add distance covered in this step to the absolute distance
            currentAngle = rowVectorMultVector(C, xNext);

            absoluteAngle += currentAngle; //czy na pewno jest to wartość w kolejnym korku pętli czy też absolutna?

            // TX_size = sprintf (TX_data, "A_X = %i \t A_Y = %i \t A_Z = %i \t||\t G_R = %i \t G_P = %i \t G_Y = %i \t || ENC=%i\n\r", IMU_rawdata[2], IMU_rawdata[0], IMU_rawdata[1], IMU_rawdata[5], IMU_rawdata[3], IMU_rawdata[4], htim4.Instance->CNT);
            //  HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
            // HAL_Delay(100);

            // while(!Btn_IsPressed(BTN1_Pin)){}
            float temp1 = absoluteAngle * FLOAT_TO_INT;
            float temp2 = y * FLOAT_TO_INT;
            float temp3 = currentAngle * FLOAT_TO_INT;
            float temp4 = (y - currentAngle) * FLOAT_TO_INT;
            //float temp1 = 1.234;
            //UART_Send_String("<.>\n\r");

            //TX_size = sprintf(TX_data, "abs. dist. = %u.%03u \t\t| meas. dist. = %u.%03u \t\t| est. dist. = %u.%03u \t\t| meas. adj = %u.%03u \n\r", absoluteDistance, y, angle, y - angle);
            TX_size = sprintf(TX_data,
                              "abs. angle. = %u.%05u \t| meas. angle. = %u.%03u \t| est. angle. = %u.%03u \t| meas. adj = %u.%03u\n\r",
                              (int) temp1 / FLOAT_TO_INT, (int) temp1 % FLOAT_TO_INT, (int) temp2 / FLOAT_TO_INT,
                              (int) temp2 % FLOAT_TO_INT, (int) temp3 / FLOAT_TO_INT, (int) temp3 % FLOAT_TO_INT,
                              (int) temp4 / FLOAT_TO_INT, (int) temp4 % FLOAT_TO_INT);

            //TX_size = UART_TX_BUFFOR_SIZE;
            //TX_size = sprintf(TX_data, "DATA SIZE = %i\n\r", TX_size);
            HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
            HAL_Delay(100);
            //czy adjustment jest względem dystansu całkowitego (jeśli filtr mierzy całkowity) czy też względem krokowego (jeśli mierzy stan w danym kroku - skłaniam się ku tej opcji !sprawdzić!

            //while(!Btn_IsPressed(BTN3_Pin)){}

            if (absoluteAngle >= 360) {

                UART_Send_String(">wykonalem obrot 360 deg\n\r");
                Motors_Stop();

                freeMatrix(&A);
                freeMatrix(&V);
                freeMatrix(&pPrev);
                freeMatrix(&pNext);
                freeVector(&B);
                freeVector(&C);
                freeVector(&xPrev);
                freeVector(&xNext);
                freeVector(&K);
                freeVector(&tempVector1);
                freeVector(&tempVector2);
                freeMatrix(&tempMatrix1);
                freeMatrix(&tempMatrix2);
                freeMatrix(&tempMatrix3);
                break;
            }
            //delay function here (to get new measurements, e.g. 100ms)
            //TX_size = sprintf(TX_data, "koniec petli nr = %i\n\r", counter);
            //HAL_UART_Transmit_IT(&uart4, TX_data, TX_size);
            setDelay(LOOP_DELAY);
        }
    }
}
