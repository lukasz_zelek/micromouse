/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "Algorithms/Flood_fill/floodfill.h"
#include "Diagnostic/debug.h"
#include "Diagnostic/debug_gui.h"
#include "Foundations/mpu6050.h"
#include "Foundations/basic_io.h"
#include "Foundations/imu.h"
#include "Logic/Control/wall_detection.h"
#include "Logic/Control/motors.h"
#include "Logic/Memory/mapsave.h"

/* USER CODE END Includes */

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc3;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart4;
DMA_HandleTypeDef hdma_uart4_tx;
DMA_HandleTypeDef hdma_uart4_rx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
//USART CODE
static uint16_t cnt = 0; // Licznik wyslanych wiadomosci
uint8_t RX_data[UART_RX_BUFFOR_SIZE]; // Tablica do obdioru
uint8_t TX_data[UART_TX_BUFFOR_SIZE]; // Tablica do wysylania
uint16_t TX_size = 0; // Rozmiar wysylanej wiadomosci
uint8_t FLAG_UART_RECIVED = 0;
int16_t IMU_rawdata[6];

//Algorithm
microMouseState robotState;
uint8_t MAP_READY = 0;
uint8_t MAZE_COMPLETE = 0;

// Globalne rejestry wartosci ADC
//uint32_t  PomiarADC;
uint32_t DistSensorWynikA[6];
uint32_t DistSensorWynikB[6];
uint32_t DSW[6];
uint32_t *DistSensorWynik = (uint32_t *) DistSensorWynikA;

uint32_t AmbientSensorWynikA[6];
uint32_t AmbientSensorWynikB[6];
uint32_t *AmbientSensorWynik = (uint32_t *) AmbientSensorWynikA;

uint8_t FLAG_WALL_FRONT = 0;
uint8_t FLAG_WALL_LEFT = 0;
uint8_t FLAG_WALL_RIGHT = 0;

extern uint32_t DIST_WALL_DISTANCE_LEFT;
extern uint32_t DIST_WALL_DISTANCE_RIGHT;
extern uint32_t DIST_WALL_DISTANCE_FRONTR;
extern uint32_t DIST_WALL_DISTANCE_FRONTL;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

static void MX_GPIO_Init(void);

static void MX_DMA_Init(void);

static void MX_ADC3_Init(void);

static void MX_ADC1_Init(void);

static void MX_UART4_Init(void);

static void MX_TIM6_Init(void);

static void MX_TIM2_Init(void);

static void MX_TIM4_Init(void);

static void MX_I2C2_Init(void);

static void MX_TIM7_Init(void);

static void MX_TIM3_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

//PRZERWANIE - odniór danych z uarta
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
    HAL_UART_Receive_DMA(huart, RX_data, 1);
    FLAG_UART_RECIVED = 1;
}

//TODO Uruchomic przerwania dla ADC1
//void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
//
//  //Wylacz skanowanie
//  if(hadc->Instance == ADC1){
//    DistSensotTurnOffAll();
//    LED_Turn_On(LED3_Pin);
//  }
//
//}

//Przerwania timerow
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
    if (htim->Instance == TIM6) { // Przerwania od cyklicznego skanowania czujników
        //LED_Toggle(LED4_Pin);
        DistSensorTimerCallback();
    }
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void initMaping() {
    //inicjalizacja zmiennych poczatkowych
    for (int i = 0; i < MAP_SIZE_N; i++)
        for (int j = 0; j < MAP_SIZE_N; j++)
            robotState.flooding[i][j] = 0xff;
    for (int i = 0; i < MAP_SIZE_N; i++)
        for (int j = 0; j < MAP_SIZE_N; j++)
            robotState.mappedArea[i][j] = 0;
    robotState.visited[0] = TRUE;
    for (int i = 1; i <= 0xff; i++)
        robotState.visited[i] = FALSE;
    //next to explore will be initialize after first map
    robotState.step = 0;
    robotState.target = -1;
    robotState.x = 0;
    robotState.y = 0;
    robotState.rotation = 0;
    //actual visit state
    //actual sensors state
    robotState.mazeTypeRightHanded = -1;
    robotState.nodeFound = FALSE;
    robotState.pathReturn = FALSE;
    robotState.mapIsFinished = FALSE;
}

void btout(){
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(1);
}

void printMap(){

    for (int i = 0; i < MAP_SIZE_N; i++)
    {
        for(int j = 0; j < MAP_SIZE_N; j++)
            if(i == robotState.y && j == robotState.x) {
                TX_size = sprintf(TX_data, "X");
                btout();
            }
            else
                switch(robotState.visited[i*16+j])
                {
                    case TRUE:
                        TX_size = sprintf(TX_data, "8");btout();
                        break;
                    case FALSE:
                        TX_size = sprintf(TX_data, "0");btout();
                        break;
                }
        TX_size = sprintf(TX_data, "\n\r");btout();
    }
    HAL_Delay(100);
    for (int i = 0; i < MAP_SIZE_N; i++)
    {
        for(int j = 0; j < MAP_SIZE_N; j++)
        {
            TX_size = sprintf(TX_data, "%4i", robotState.flooding[i][j]);btout();
        }
        TX_size = sprintf(TX_data, "\n\r");btout();
    }
    HAL_Delay(100);
    for (int i = 0; i < MAP_SIZE_N; i++)
    {
        for(int j = 0; j < MAP_SIZE_N; j++)
        {
            TX_size = sprintf(TX_data, "%i%i%i%i ", robotState.mappedArea[i][j]&8,robotState.mappedArea[i][j]&4,robotState.mappedArea[i][j]&2,robotState.mappedArea[i][j]&1);btout();
        }
        TX_size = sprintf(TX_data, "\n\r");btout();
    }
    HAL_Delay(100);
}

void printRobotState()
{
    printMap();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.nextToExplore);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.step);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.target);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.x);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.y);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.rotation);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.actualVisitState);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.actualSensorsState);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.mazeTypeRightHanded);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.nodeFound);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.pathReturn);btout();
    HAL_Delay(10);
    TX_size = sprintf(TX_data, "%d\n\r",robotState.mapIsFinished);btout();
}

void testQuery()
{
    HAL_UART_Receive_DMA(&huart4, RX_data, 1);
    initMaping();
    while(!MAZE_COMPLETE)
    {
        int input = 0;
        for(int i = 0; i < 3; i++)
        {
            if(!robotState.mapIsFinished)
                while (!FLAG_UART_RECIVED){}
            if(FLAG_UART_RECIVED) {
                FLAG_UART_RECIVED = 0;
                int beacon = 0;
                if(RX_data[0] == '1')
                    beacon = 1;
                if(RX_data[0]=='0')
                    beacon = 0;
                input = ((beacon << (2 - i)) | input);
            }
        }
        robotState.actualSensorsState = input;
        int answer = -1;
        if (robotState.mapIsFinished == FALSE) {
            answer = query(&robotState);

        } else if (MAZE_COMPLETE == FALSE) {
            if (!MAP_READY) {
                query(&robotState);
                MAP_READY = 1;

            }
            answer = goFastestWay(&robotState);
            if (robotState.y * MAP_SIZE_N + robotState.x == robotState.target) {
                MAZE_COMPLETE = 1;
            }
        }
        switch(answer){
            case 0:
                TX_size = sprintf(TX_data, "TURN AROUND LEFT\n\r");btout();
                break;
            case 1:
                TX_size = sprintf(TX_data, "TURN LEFT\n\r");btout();
                break;
            case 2:
                TX_size = sprintf(TX_data, "FORWARD\n\r");btout();
                break;
            case 3:
                TX_size = sprintf(TX_data, "TURN RIGHT\n\r");btout();
                break;
            case 4:
                TX_size = sprintf(TX_data, "TURN AROUND RIGHT\n\r");btout();
                break;
            case 6:
                TX_size = sprintf(TX_data, "MAZE MAPPED\n\r");btout();
                break;
            case 7:
                TX_size = sprintf(TX_data, "END QUERY\n\r");btout();
                break;
        }
        printMap();
    }
}

void controller(int fb){
    switch (fb) {
        case -1:
            break;
        case 0:
            //obrot o 180 stopni w lewo i kratka do przodu
            Motors_Turn_Left90Degrees();
            Motors_Turn_Left90Degrees();
            Motors_Forward();
            break;
        case 1:
            //obrot w lewo 90 stopni i kratka do przodu
            Motors_Turn_Left90Degrees();
            Motors_Forward();
            break;
        case 2:
            //kratka do przodu
            Motors_Forward();
            break;
        case 3:
            //obrot w prawo 90 stopni i kratka do przodu
            Motors_Turn_Right90Degrees();
            Motors_Forward();
            break;
        case 4:
            //obrot o 180 stopni w prawo i kratka do przodu
            Motors_Turn_Right90Degrees();
            Motors_Turn_Right90Degrees();
            Motors_Forward();
            break;
        default:
            break;
    }
}

void saveTest()
{
    Memory_MapLoad(&robotState);
    printRobotState();
}

void testSavingRobotState()
{
    for (int i = 0; i < MAP_SIZE_N; i++)
        for (int j = 0; j < MAP_SIZE_N; j++)
            robotState.flooding[i][j] = MAP_SIZE_N * i + j;
    for (int i = 0; i < MAP_SIZE_N; i++)
        for (int j = 0; j < MAP_SIZE_N; j++)
            robotState.mappedArea[i][j] = (MAP_SIZE_N * i + j)%MAP_SIZE_N;
    robotState.visited[0] = TRUE;
    for (int i = 1; i <= 0xff; i++)
        robotState.visited[i] = i%2;
    //next to explore will be initialize after first map
    robotState.step = 7;
    robotState.target = 136;
    robotState.x = 6;
    robotState.y = 5;
    robotState.rotation = 4;
    //actual visit state
    //actual sensors state
    robotState.mazeTypeRightHanded = 1;
    robotState.nodeFound = TRUE;
    robotState.pathReturn = FALSE;
    robotState.mapIsFinished = TRUE;

    printRobotState();

    Memory_MapSave(&robotState);
    HAL_Delay(2000);

    initMaping();

    saveTest();
}

void onlyOneFastRunToTarget()
{
    //ladowanie stanu robota z pamieci
    //Memory_MapLoad(&robotState);
    //start Robota - 3 2 1 0
    LED_Turn_OnAll();
    HAL_Delay(100);
    LED_Turn_Off(LED1_Pin);
    HAL_Delay(100);
    LED_Turn_Off(LED2_Pin);
    HAL_Delay(100);
    LED_Turn_Off(LED3_Pin);
    HAL_Delay(100);
    LED_Turn_Off(LED4_Pin);
    WallCheckCallibration();
    DistSensorTimerStart();

    while(!MAZE_COMPLETE){
        controller(goFastestWay(&robotState));
        if (robotState.y * MAP_SIZE_N + robotState.x == robotState.target) {
            MAZE_COMPLETE = 1;
        }
    }
    if (MAZE_COMPLETE == 1) {
        LED_Turn_OnAll();
    }
}

void robotGetYourAssUp() {
    //inicjalizacja mapowania
    initMaping();
    //start Robota - 3 2 1 0
    LED_Turn_OnAll();
    HAL_Delay(100);
    LED_Turn_Off(LED1_Pin);
    HAL_Delay(100);
    LED_Turn_Off(LED2_Pin);
    HAL_Delay(100);
    LED_Turn_Off(LED3_Pin);
    HAL_Delay(100);
    LED_Turn_Off(LED4_Pin);
    WallCheckCallibration();
    DistSensorTimerStart();

    while (1) {
        int feedback = -1;
        robotState.actualSensorsState = 0;
        if(!robotState.mapIsFinished)
            robotState.actualSensorsState = ((1u * FLAG_WALL_LEFT << 2u) | (1u * FLAG_WALL_FRONT << 1u) |
                                         1u * FLAG_WALL_RIGHT);
        //TX_size = sprintf(TX_data, "%d %d %d\n\r", FLAG_WALL_LEFT, FLAG_WALL_FRONT, FLAG_WALL_RIGHT);btout();
        if (robotState.mapIsFinished == FALSE) {
            feedback = query(&robotState);

        } else if (MAZE_COMPLETE == FALSE) {
            if (!MAP_READY) {
                query(&robotState);
                //LED_Turn_On(LED3_Pin);
                //HAL_Delay(10);
                //LED_Turn_Off(LED3_Pin);
                Memory_MapSave(&robotState);
                MAP_READY = 1;
            }
            else
            {
                //LED_Turn_On(LED1_Pin);
                //HAL_Delay(10);
                //LED_Turn_Off(LED1_Pin);
                //feedback = goFastestWay(&robotState);
                //if (robotState.y * MAP_SIZE_N + robotState.x == robotState.target) {
                //    MAZE_COMPLETE = 1;
                //}
                while (!BTN_IsPressed(BTN3_Pin) && !BTN_IsPressed(BTN2_Pin) && !BTN_IsPressed(BTN1_Pin)){}
                if (BTN_IsPressed(BTN3_Pin)) {
                    onlyOneFastRunToTarget();
                    while (1) {}
                }
            }
        }
        controller(feedback);
        if (MAZE_COMPLETE == TRUE) {
            LED_Turn_OnAll();
            break;
        }
    }
    DistSensorTimerStop();
}

void debuging() {
    //Rozpoczecie nasluchiwania na dane z wykorzystaniem DMA
    HAL_UART_Receive_DMA(&huart4, RX_data, 1);
    LED_Turn_OffAll();
    LED_Turn_On(LED3_Pin);

    DEBUG_MainScreen();

    while (1) {

        if (FLAG_UART_RECIVED) {
            FLAG_UART_RECIVED = 0;
            switch (RX_data[0]) {

                case '1':
                    DEBUG_Leds();
                    break;

                case '2':
                    DEBUG_BattVoltage();
                    break;

                case '3':
                    DEBUG_Butons();
                    break;

                case '4':
                    DEBUG_DistSensors();
                    break;

                case '5':
                    IMU_MPU6050_Init();
                    DEBUG_IMU();
                    break;

                case '6':
                    DEBUG_Motors();
                    break;

                case '7':
                    DEBUG_Memory();
                    break;

                case '8':
                    DEBUG_I2C();
                    break;

                case 'a':
                    DEBUG_Soft_WallDetection();
                    break;

                case 'b':
                    IMU_MPU6050_Init();
                    DEBUG_Soft_KalmanPosition();
                    FLAG_UART_RECIVED = 0;
                    break;

                case 'c':
                    DEBUG_Soft_Motors_PID();
                    break;

                case 'd':
                    DEBUG_Soft_KalmanAngle();
                    break;

                case 'e':
                    DEBUG_Soft_Mapping();
                    break;

                case 'f':
                    DEBUG_Soft_IMUProcessing();
                    break;

                case 'g':
                    DEBUG_Soft_MemInfo();
                    break;

                case 'h':
                    DEBUG_Soft_DataExtractor();
                    break;

                case 'j':
                    DEBUG_Soft_Motors_Rotate();
                    break;

                case 'x':

                    //DistSensorTurnOn(ADC_DIST_LEFT_SIDE);
                    //DistSensorTurnOn(ADC_DIST_RIGHT_SIDE);
                    DistSensorSetSensor(ADC_DIST_RIGHT_SIDE);
                    HAL_ADC_Start(&hadc1);
                    uint32_t buforek = HAL_ADC_GetValue(&hadc1);
//                        while (HAL_ADC_PollForConversion(&hadc1, 50) != HAL_OK) {
//                        }
//                        buforek = HAL_ADC_GetValue(&hadc1);
                    HAL_Delay(1000);
                    //DistSensorTurnOff(ADC_DIST_LEFT_SIDE);
                    DistSensorTurnOn(ADC_DIST_RIGHT_SIDE);
                    HAL_Delay(1000);
                    UART_Send_String("DONE.\n\r");
                    HAL_ADC_Start(&hadc1);
                    uint32_t buforek_on = HAL_ADC_GetValue(&hadc1);
                    TX_size = sprintf(TX_data, "BUFOREK: %i AFTER: %i\n\r", buforek, buforek_on);
                    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
                    HAL_Delay(1000);
                    DistSensorTurnOff(ADC_DIST_RIGHT_SIDE);
                    break;


                default: // Jezeli odebrano nieobslugiwany znak
                    UART_Send_String(">Unknown symbol\n\r\n\r");
                    break;
            }//SWITCH
            DEBUG_MainScreen();
        }//IF
        //Nadaj odpowiedź po UARTcie
        HAL_Delay(50);

    }//WHILE
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void) {
    /* USER CODE BEGIN 1 */

    /* USER CODE END 1 */

    /* MCU Configuration----------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    ///remap pinow na DEBUG
    //__HAL_AFIO_REMAP_SWJ_ENABLE();
    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_ADC3_Init();
    MX_ADC1_Init();
    MX_UART4_Init();
    MX_TIM6_Init();
    MX_TIM2_Init();
    MX_TIM4_Init();
    MX_I2C2_Init();
    MX_TIM7_Init();
    MX_TIM3_Init();
    /* USER CODE BEGIN 2 */

    //Pin informujacy o zasilaniu
    LED_Turn_OffAll();
    LED_Turn_On(LED1_Pin);

    //Uruchomienie enkoderow
    HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
    HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);
    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */

    DistSensorTimerStart();
    HAL_UART_Receive_DMA(&huart4, RX_data, 1);

    //Oczekiwanie na wciśnięcie przycisku lub sygnal po bluetooth
    while (!BTN_IsPressed(BTN3_Pin) && !BTN_IsPressed(BTN2_Pin) && !BTN_IsPressed(BTN1_Pin) && !FLAG_UART_RECIVED) {}

    //sygnal 's' po bluetooth -> DEBUG GUI

    if (FLAG_UART_RECIVED) {
        FLAG_UART_RECIVED = 0;
        LED_Turn_OffAll();
        switch (RX_data[0]) {
            case 's':
                Debug_GUI();
                break;
            case 'd':
                debuging();
                break;
            case 'r':
                HAL_UART_DMAStop(&huart4);
                robotGetYourAssUp();
                break;
            case 'a':
                testQuery();
                break;
            case 'p':
                saveTest();
                break;
            case 't':
                testSavingRobotState();
                break;
            case 'm':
                while(!FLAG_UART_RECIVED)
                    Motors_Forward();
                break;
        }
        while (!BTN_IsPressed(BTN3_Pin) && !BTN_IsPressed(BTN2_Pin) && !BTN_IsPressed(BTN1_Pin)) {}
    }

    //DEBUG
    if (BTN_IsPressed(BTN2_Pin)) {
        //debuging();
        HAL_UART_DMAStop(&huart4);
        onlyOneFastRunToTarget();
        while(1){}
    }//IF
        //INIT
    else if (BTN_IsPressed(BTN1_Pin)) {
        LED_Turn_OffAll();
        LED_Turn_On(LED2_Pin);
        //int err = 0;
        //uint16_t encoderRight, encoderLeft;
        HAL_Delay(1000);
        HAL_Delay(1000);
        HAL_Delay(1000);
        HAL_Delay(1000);
        HAL_Delay(1000);
        //DistSensorTimerStart();
        Motors_Init();
        while (!BTN_IsPressed(BTN2_Pin)) {

//          err = DistSensorWynik[LEFT_DIAGONAL] - DistSensorWynik[RIGHT_DIAGONAL];
//          if(err < 0){
//              err = -err;
//              err = (int)sqrt((double)err);
//              err = -err;
//          } else{ err = (int)sqrt((double)err); }
//          if(err > 10){err =10;}
//          if(err < -10){err =-10;}


            if (FLAG_WALL_FRONT == 0) {
                Motors_Forward();
                HAL_Delay(50);
            } else if (FLAG_WALL_FRONT == 1) {

                HAL_Delay(200);
                if (FLAG_WALL_RIGHT == 1 && FLAG_WALL_LEFT == 1) {
                    LED_Turn_On(LED3_Pin);
                    //Motors_Turn_Around();
                    Motors_Turn_Right90Degrees();
                    Motors_Backward();
                    Motors_Forward();
                    Motors_Turn_Right90Degrees();
                    Motors_Backward();
                    Motors_Init();
                } else if (FLAG_WALL_LEFT) {
                    Motors_Turn_Right90Degrees();
                    Motors_Backward();
                    Motors_Init();
                } else if (FLAG_WALL_RIGHT) {
                    Motors_Turn_Left90Degrees();
                    Motors_Backward();
                    Motors_Init();
                } else {
                    Motors_Turn_Right90Degrees();
                    Motors_Init();
                }
            }
        }
        Motors_Stop();
        DistSensorTimerStop();
    } else if (BTN_IsPressed(BTN3_Pin)) {
        HAL_UART_DMAStop(&huart4);
        robotGetYourAssUp();
    }
    while (1) {}


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


    /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void) {

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**
     * Initializes the CPU, AHB and APB busses clocks
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                  | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
    PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure the Systick interrupt time 
    */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

    /**Configure the Systick 
    */
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void) {

    ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
    hadc1.Instance = ADC1;
    hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
    hadc1.Init.ContinuousConvMode = DISABLE;
    hadc1.Init.DiscontinuousConvMode = DISABLE;
    hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc1.Init.NbrOfConversion = 1;
    if (HAL_ADC_Init(&hadc1) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure Regular Channel 
    */
    sConfig.Channel = ADC_CHANNEL_10;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

}

/* ADC3 init function */
static void MX_ADC3_Init(void) {

    ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
    hadc3.Instance = ADC3;
    hadc3.Init.ScanConvMode = ADC_SCAN_DISABLE;
    hadc3.Init.ContinuousConvMode = DISABLE;
    hadc3.Init.DiscontinuousConvMode = DISABLE;
    hadc3.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc3.Init.NbrOfConversion = 1;
    if (HAL_ADC_Init(&hadc3) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure Regular Channel 
    */
    sConfig.Channel = ADC_CHANNEL_0;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
    if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

}

/* I2C2 init function */
static void MX_I2C2_Init(void) {

    hi2c2.Instance = I2C2;
    hi2c2.Init.ClockSpeed = 100000;
    hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
    hi2c2.Init.OwnAddress1 = 0;
    hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c2.Init.OwnAddress2 = 0;
    hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c2) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

}

/* TIM2 init function */
static void MX_TIM2_Init(void) {

    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    TIM_OC_InitTypeDef sConfigOC;

    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 71;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 999;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    if (HAL_TIM_PWM_Init(&htim2) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    HAL_TIM_MspPostInit(&htim2);

}

/* TIM3 init function */
static void MX_TIM3_Init(void) {

    TIM_Encoder_InitTypeDef sConfig;
    TIM_MasterConfigTypeDef sMasterConfig;

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 0;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 65535;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
    sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
    sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
    sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
    sConfig.IC1Filter = 15;
    sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
    sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
    sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
    sConfig.IC2Filter = 15;
    if (HAL_TIM_Encoder_Init(&htim3, &sConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

}

/* TIM4 init function */
static void MX_TIM4_Init(void) {
    TIM_Encoder_InitTypeDef sConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim4.Instance = TIM4;
    htim4.Init.Prescaler = 0;
    htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim4.Init.Period = 65535;
    htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
    sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
    sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
    sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
    sConfig.IC1Filter = 15;
    sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
    sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
    sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
    sConfig.IC2Filter = 15;
    if (HAL_TIM_Encoder_Init(&htim4, &sConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
}

/* TIM6 init function */
static void MX_TIM6_Init(void) {
    TIM_MasterConfigTypeDef sMasterConfig;
    htim6.Instance = TIM6;
    htim6.Init.Prescaler = VRISKER_TIM6_PRESCALER;
    htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim6.Init.Period = VRISKER_TIM6_PERIOD;
    htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim6) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
}

/* TIM7 init function */
static void MX_TIM7_Init(void) {
    TIM_MasterConfigTypeDef sMasterConfig;
    htim7.Instance = TIM7;
    htim7.Init.Prescaler = 35999;
    htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim7.Init.Period = 1999;
    htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim7) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
}

/* UART4 init function */
static void MX_UART4_Init(void) {
    huart4.Instance = UART4;
    huart4.Init.BaudRate = 115200;
    huart4.Init.WordLength = UART_WORDLENGTH_8B;
    huart4.Init.StopBits = UART_STOPBITS_1;
    huart4.Init.Parity = UART_PARITY_NONE;
    huart4.Init.Mode = UART_MODE_TX_RX;
    huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart4.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart4) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }
}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) {
    /* DMA controller clock enable */
    __HAL_RCC_DMA1_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

    /* DMA interrupt init */
    /* DMA1_Channel1_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
    /* DMA2_Channel3_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Channel3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Channel3_IRQn);
    /* DMA2_Channel4_5_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Channel4_5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Channel4_5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through 
        * the Code Generation settings)
*/
static void MX_GPIO_Init(void) {

    GPIO_InitTypeDef GPIO_InitStruct;

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, MOTOR_LEFT_BWRD_Pin | MOTOR_LEFT_FWD_Pin, GPIO_PIN_SET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOC, LED1_Pin | LED2_Pin | LED3_Pin | LED4_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, DIST1_EN_Pin | DIST2_EN_Pin | DIST3_EN_Pin | DIST4_EN_Pin
                             | DIST5_EN_Pin | DIST6_EN_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, MOTOR_RIGHT_BWRD_Pin | MOTOR_RIGHT_FWD_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins : PC13 PC14 PC15 PC12 */
    GPIO_InitStruct.Pin = GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15 | GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /*Configure GPIO pins : PA3 PA4 PA5 */
    GPIO_InitStruct.Pin = GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : BTN1_Pin BTN2_Pin BTN3_Pin */
    GPIO_InitStruct.Pin = BTN1_Pin | BTN2_Pin | BTN3_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : MOTOR_LEFT_BWRD_Pin MOTOR_LEFT_FWD_Pin MOTOR_RIGHT_BWRD_Pin MOTOR_RIGHT_FWD_Pin */
    GPIO_InitStruct.Pin = MOTOR_LEFT_BWRD_Pin | MOTOR_LEFT_FWD_Pin | MOTOR_RIGHT_BWRD_Pin | MOTOR_RIGHT_FWD_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : PB14 PB15 PB4 PB5 */
    GPIO_InitStruct.Pin = GPIO_PIN_14 | GPIO_PIN_15 | GPIO_PIN_4 | GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : LED1_Pin LED2_Pin LED3_Pin LED4_Pin */
    GPIO_InitStruct.Pin = LED1_Pin | LED2_Pin | LED3_Pin | LED4_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /*Configure GPIO pins : DIST1_EN_Pin DIST2_EN_Pin DIST3_EN_Pin DIST4_EN_Pin
                             DIST5_EN_Pin DIST6_EN_Pin */
    GPIO_InitStruct.Pin = DIST1_EN_Pin | DIST2_EN_Pin | DIST3_EN_Pin | DIST4_EN_Pin
                          | DIST5_EN_Pin | DIST6_EN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : PD2 */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /*Configure GPIO pin : INT_GYRO_Pin */
    GPIO_InitStruct.Pin = INT_GYRO_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(INT_GYRO_GPIO_Port, &GPIO_InitStruct);

    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

void _Error_Handler(char *file, int line) {
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    while (1) {
        LED_Turn_OnAll();
        HAL_Delay(500);
        LED_Turn_OffAll();
        HAL_Delay(500);
    }
    /* USER CODE END Error_Handler_Debug */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line);

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

#pragma clang diagnostic pop
