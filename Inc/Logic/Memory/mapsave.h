//
// Created by Mad Scientist on 06.04.2019.
//

#ifndef VRIKSER_MAPSAVE_H
#define VRIKSER_MAPSAVE_H

#include "Algorithms/Flood_fill/floodfill.h"
#include "Foundations/eeprom.h"

void Memory_MapSave(microMouseState* savedata);
void Memory_MapLoad(microMouseState* loaddata);

#endif //VRIKSER_MAPSAVE_H
