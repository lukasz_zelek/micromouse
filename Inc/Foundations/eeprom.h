//
// Created by mad on 17.12.18.
//

#ifndef VRIKSER_EEPROM_H
#define VRIKSER_EEPROM_H

#include "stm32f1xx_hal.h"
#include "main.h"

void EEPROM_Write(uint16_t addr, uint8_t* data);
void EEPROM_Write_Bytes(uint16_t addr, uint8_t* pData, uint8_t size); //max 64 bajty
void EEPROM_Read(uint16_t addr, uint8_t* buffer);
void EEPROM_Read_Bytes(uint16_t addr, uint8_t* pBuffer, uint8_t size); //max 64 bajty

#endif //VRIKSER_EEPROM_H
